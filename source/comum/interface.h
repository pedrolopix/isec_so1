#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sem.h>

#define FIFO_LEN 20  //tamanho do nome pipe
#define FIFO_SERVIDOR "pserver"
#define FIFO_CLIENTE  "pcli%d"  //%d -> pid do cliente
#define ERROR_LEN 80
#define SEM_MSG 21200565
#define SEM_ONLINE 21080521
#define SHM_ONLINE 211805211
#define MAXUSERS 30

enum commands {c_register,c_login,c_logoff,c_remove,c_exit,c_write,c_status,c_server_exit};

typedef struct _pedido{
	enum commands comando;
	char str1[256];
	char str2[256];
	int pid;
} pedido, *ppedido;

union semun {
  int              val;    /* Value for SETVAL */
  struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
  unsigned short  *array;  /* Array for GETALL, SETALL */
  struct seminfo  *__buf;  /* Buffer for IPC_INFO
                           (Linux-specific) */
};

typedef struct _useronline {
    char username[21];
    int pid;
} USERONLINE, *PUSERONLINE;

typedef struct _useronlinectrl {
    USERONLINE users[MAXUSERS];
    int numusers;
} USERONLINECTRL, *PUSERONLINECTRL;

int semaforoInit(int value, int key, int flg);
void semaforo(int semid, int value);
void getError(int ret, char *msg);
void checkError(int ret,int termina);
int quebra_frase(char frase[],char *comandos[], int tam);
