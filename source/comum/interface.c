#include "interface.h"


int semaforoInit(int value, int key, int flg)
{
  int ret;
  ret=semget(key,1,flg);
  if (ret==-1) {
     checkError(15,1);
  }
  if (semctl(ret,0,SETVAL,value)==-1) {
     checkError(15,1);
  }
  return ret;
}

void semaforo(int semid, int value)
{
  struct sembuf var;
  var.sem_num=0;
  var.sem_flg=SEM_UNDO;
  var.sem_op=value;
  semop(semid,&var,1);
}


void getError(int ret, char *msg){

   char *erros[ERROR_LEN]={"0. Operação com sucesso",
      "1. Erro a criar ficheiro de utilizadores",
      "2. Erro a gravar ficheiro, tamanho de bytes lidos incorrectos",
      "3. Erro a abrir ficheiro de utilizadores",
      "4. Erro a ler ficheiro, tamanho de bytes lidos incorrectos",
      "5. Palavra-passe errada",
      "6. Não existe o utilizador registado",
      "7. Número máximo de utilizadores atingidos",
      "8. Username reservado",
      "9. Utilizador não está autenticado",
      "10. Já existe o fifo criado do servidor",
      "11. Erro a criar fifo no servidor - erro do SO",
      "12. Erro a abrir o fifo para leitura no servidor",
      "13. Erro a ler o fifo no servidor",
      "14. Erro a redirecionar o sinal",
      "15. Erro na inicialização do semaforo",
      "16. O Servidor já está aberto",
      "17. Erro a abrir o fifo do servidor no cliente",
      "18. Erro a gravar no fifo do servidor",
      "19. Erro a criar fifo do cliente",
      "20. Erro a abrir o fifo de leitura do cliente",
      "21. Comando desconhecido",
      "22. Erro a escrever no fifo do cliente",
      "23. Erro a criar memória partilhada",
      "24. Erro a mapear memória partilhada",
      "25. O utilizador não está online",
      "26. Erro a criar fork()"
      };
   strcpy(msg,erros[ret]);
}

void checkError(int ret,int termina) {
  char erro[ERROR_LEN];
  if (ret==0) return;
  getError(ret,erro);
  if (termina==1) {
    perror(erro);
    exit(ret);
  } else {
    printf("Erro %s",erro);
  }
}

int quebra_frase(char frase[],char *comandos[],int tam)
{
  int i=0;
  int j=1;
  comandos[0]=&frase[0];
  for (i=0;frase[i]!='\0';i++){
    if (frase[i]==' ' || frase[i]=='\n' || frase[i]=='\t' )  {
       frase[i]='\0';
       comandos[j++]=&(frase[i+1]);
    }
  }
 j++;
 for (i=j;i<tam;i++) comandos[i]='\0';
 return j;
}

