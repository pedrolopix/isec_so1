#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "../comum/interface.h"

typedef struct _cliente {
   int pid;
   int server_fifo;
   int cliente_fifo;
   int sem_msg;
   int loggedin;  //0 ou 1
} CLIENT, *PCLIENT;

CLIENT cliente;

int enviar_pedido(enum commands comando, char *str1, char *str2){
   	int ret;
   	//printf("a enviar comando: %s:%s\n",str1,str2);
   	pedido ped;
   	ped.pid=cliente.pid;
   	ped.comando=comando;
    strcpy(ped.str1,str1);
    strcpy(ped.str2,str2);
    //envia para o servidor
    ret=write(cliente.server_fifo, &ped, sizeof(pedido));
    if (ret<=0) checkError(18,1);

    //enviar sinal
    semaforo(cliente.sem_msg,1);
    //printf("comando enviado\n");
    return 0;
}

void terminar() {
    close(cliente.server_fifo);
    close(cliente.cliente_fifo);
    printf("Terminado\n");
    exit(0);
}

int cmd_registar() {
    int i;
    char fifo_name[FIFO_LEN];
    printf("A registar no servidor...\n");
    if((i=open(FIFO_SERVIDOR,O_RDWR)) == -1){
		return 17;
    }

    cliente.server_fifo=i;

    //criar fifo local caso não exista
    sprintf(fifo_name,FIFO_CLIENTE,cliente.pid);
    if (access(fifo_name, F_OK)){
        if (mkfifo(fifo_name, 0777)!= 0)
        {
            return 19;
        }
    }
    if ((cliente.cliente_fifo = open(fifo_name, O_RDWR)) == -1){
      return 12;
    }
    //envia registo ao servidor
    enviar_pedido(c_register,"registo","");
    return 0;
}

int cmd_login(char *username, char *password) {
  if (cliente.loggedin==1){
    printf("\nNão é possível fazer login ja existindo um cliente logado!\n");
    return 0;}
  if (strlen(username)==0 || strlen(password)==0) {
    printf("comando incorrecto, usar na forma: login <username> <password>\n");
    return 1;
  }
  enviar_pedido(c_login,username,password);
  return 0;
}

int cmd_logoff() {
  if (cliente.loggedin==0){
    printf("\nNão é possível fazer logoff sem estar logado!\n");
    return 0;}
  printf("logoff.\n");
  enviar_pedido(c_logoff,"","");
  cliente.loggedin=0;
  return 0;
}

int cmd_remove() {
  char escolha[5];
  if (cliente.loggedin==0){
    printf("\nNão é possível remover sem estar logado!\n");
    return 0;}

  do{
    printf("\nTem a certeza que pretende remover? (sim/nao): ");
    fgets(escolha,5,stdin);
    if (strcmp(escolha,"sim\n")==0) {
      enviar_pedido(c_remove,"","");
      break;
     }
     if (strcmp(escolha,"nao\n")==0) return 0;
  } while (1);
  return 0;
}

int cmd_exit() {
    enviar_pedido(c_exit,"","");
    printf("Adeus\n");
    terminar();
    return 0;
}

int cmd_write(char *destinatario,char *texto[]){
  char msg[256];
  int i;
  if (cliente.loggedin==0){
    printf("\nNão é possível escrever mensagem sem estar logado!\n");
    return 0;}
  if (strlen(destinatario)==0 || strlen(texto[2])==0) {
    printf("comando incorrecto, usar na forma: write <destinatario> <texto>\n");
    return 1;
  }
  strcpy(msg,"");
  for (i=2;strlen(texto[i])!=0;i++) strcat(strcat(msg," "),texto[i]);
  //printf(">>>>%s-%s",destinatario,msg);
  enviar_pedido(c_write,destinatario,msg);
  return 0;
}

int cmd_who(){
    PUSERONLINECTRL online;
    int shmid,sem_online;
    int i;
    if (cliente.loggedin==0){
    printf("\nNão é possível obter lista de utilizadores sem estar logado!\n");
    return 0;}

    //criar memoria partilhada users online
    shmid = shmget(SHM_ONLINE,sizeof(USERONLINECTRL), IPC_CREAT  | 0777);
    if(shmid == -1)
        checkError(23,1);
    //mapear memoria partilhada
    if ( ( (online) =(PUSERONLINECTRL) shmat(shmid,(void *) 0, 0)) == (void *) -1) {
        checkError(24,1);
    }

    sem_online = semaforoInit(1,SEM_ONLINE,IPC_CREAT|0777);

    printf("Estão ligados os utilizadores: \n");
    semaforo(sem_online,-1);
    for (i=0; i<online->numusers; i++){
        if (strlen(online->users[i].username)!=0)
          printf("%d - %s \n",i+1,online->users[i].username);
    }
    semaforo(sem_online,1);
    semctl(sem_online,0,IPC_RMID);
    shmdt(online);
    return 0;
}



void processa_msg(){
    int res;
    pedido ped;
    if ((res=read(cliente.cliente_fifo,&ped, sizeof(pedido))) == -1 ){
      checkError(13,1);
    }
    //printf("leu %d. cmd=%d\n",res,ped.comando);

    if (res>=0){
        printf("\n");
        switch (ped.comando) {
            case c_login:
                cliente.loggedin=1;
                printf("%s: %s\n",ped.str1,ped.str2);
                break;
            case c_status:
                printf("%s: %s\n",ped.str1,ped.str2);
                break;
            case c_server_exit:
                printf("%s: %s\n",ped.str1,ped.str2);
                terminar();
            default:
                printf("comando desconhecido\n");
                break;
            }


    }
    printf("\n");
}

void procSinal(int signal) {
  if (signal==SIGUSR1) {
     processa_msg();
  } else
  if (signal==SIGINT){
     cmd_exit();
  }
}

int main(){
    int ret;
    char comando[256];
    char *comandos[10];
    int ncomandos=0;
    if (signal(SIGUSR1,procSinal)==SIG_ERR) {
      checkError(14,1);
    }
    if (signal(SIGINT,procSinal)==SIG_ERR) {
      checkError(14,1);
    }
    printf("\nCliente de chat %d\n\n",getpid());
    //inica semaforo de mensagens
    cliente.sem_msg=semaforoInit(0,SEM_MSG,IPC_CREAT|0777);
    cliente.loggedin=0;
    cliente.pid = getpid();
    ret=cmd_registar();
    checkError(ret,1);

    do {
      fgets(comando,256,stdin);
      ncomandos=quebra_frase(comando,comandos,10);
      if (ncomandos==0) { continue; }
      if (strlen(comandos[0])==0 )  { continue; }
      if (strcmp(comandos[0],"login")==0) { cmd_login(comandos[1],comandos[2]); }
	  else if (strcmp(comandos[0],"logoff")==0) { cmd_logoff(); }
	  else if (strcmp(comandos[0],"remove")==0) { cmd_remove(); }
	  else if (strcmp(comandos[0],"exit")==0) { cmd_exit(); }
	  else if (strcmp(comandos[0],"write")==0) { cmd_write(comandos[1],comandos); }
	  else if (strcmp(comandos[0],"who")==0) { cmd_who(); }
      else printf("comando desconhecido!\n");
    } while (1);
    terminar();
	exit(0);
}
