#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "../comum/interface.h"

#define FICH_UTILIZADORES "USERS.DB"

typedef struct _user {
    char username[21];
    char password[21];
} USER, *PUSER;

typedef struct _userctrl {
    USER users[MAXUSERS]; // guarda os users até um máximo
    int numusers;
} USERCTRL, *PUSERCTRL;



typedef struct _server{
    PUSERCTRL registed;
    PUSERONLINECTRL online;

    int fifo;    // handle do fifo
    int online_shmid; //id para a memoria partilhada para os users online
    int users_shmid; //id para a memoria partilhada para os users registados
    int sem_users; //semaforo para a memoria partilhada
    int sem_online; //semaforo para a memoria partilhada
    int sem_msg; // semaforo para mensagens, partilhado com o cliente
} SERVER, *PSERVER;

SERVER servidor;

//grava utilizadores
//devolve
//  0-gravado com sucesso
//  1-Erro a criar ficheiro de utilizadores
//  2-Erro a gravar ficheiro, tamanho de bytes lidos incorrectos
int grava_utilizadores(USER users[],int numusers){
    int bytesescritos;
    int fd;
    fd = open(FICH_UTILIZADORES, O_WRONLY | O_CREAT, 0600); // o nome é o que for preciso
    if (fd==-1) {
        return 1;
    }
    bytesescritos = write(fd, users, sizeof(USER) * numusers);
    if (bytesescritos != sizeof(USER) * numusers)
    {
        return 2;
    }

    close(fd);
    return 0;
}

//abre utilizadores
//devolve
//  0-leitura com sucesso
//  3-Erro a abrir ficheiro de utilizadores
//  4-Erro a ler ficheiro, tamanho de bytes lidos incorrectos
int abre_utilizadores(USER users[],int *numusers){
    int fd;
    int byteslidos;
    fd = open(FICH_UTILIZADORES, O_RDONLY);
    if (fd==-1) {
        return 3;
    }
    byteslidos = read(fd, users, sizeof(USER) * MAXUSERS);
    if (byteslidos == -1)
    {
        return 4;
    }

    *numusers = byteslidos / sizeof(USER);
    close(fd);
    return 0;
}

// login do utilizador
// devolve :
//   0-login com sucesso
//   5-palavra-passe errada
//   6-não existe o utilizador registado
int login(char *username, char *password) {
  int i;
  int existe=-1;
  for(i=0;i<servidor.registed->numusers;i++){
    if (strcmp(username,servidor.registed->users[i].username)==0) {
        //utilizador existe validar password
        //printf("%d. %s - %s\n",i,servidor.registed->users[i].username,servidor.registed->users[i].password);
        existe=i;
        if (strcmp(password,servidor.registed->users[i].password)==0) {
              return 0;
            } else{
              return 5;
            }

        }
  }
  if (existe==-1) {
    return 6;
  } else
  {
    return 0;
  }
}

// adiciona utilizadores ao array server.user
// devolve:
//   0-adicionado com sucesso
//   7-numero máximo de utilizadores atingidos
//   8-Username reservado
int add_user(char *username, char *password) {
  if (servidor.registed->numusers+1>MAXUSERS){
      return 7;
  }
  if (strcmp("all",username)==0) return 8;
  if (strcmp("servidor",username)==0) return 8;

  strcpy(servidor.registed->users[servidor.registed->numusers].password,password);
  strcpy(servidor.registed->users[servidor.registed->numusers].username,username);

  //printf("Add user: %d. %s - %s\n",servidor.registed->numusers,servidor.registed->users[servidor.registed->numusers].username,servidor.registed->users[servidor.registed->numusers].password);

  servidor.registed->numusers=servidor.registed->numusers+1;
  return 0;
}

int remove_user(char *username) {
    int idx=-1;
    int i;
    for(i=0;i<servidor.registed->numusers;i++){
       if (strcmp(username,servidor.registed->users[i].username)==0) {
         idx=i;
         break;
       }
    }
    if (idx!=-1) {
       //vai buscar o ultimo
       servidor.registed->numusers--;
       if (servidor.registed->numusers>0) {
           servidor.registed->users[idx]=servidor.registed->users[servidor.registed->numusers];
       }
    }
  return 0;
}

//verifcar se o utilizador está registado
//devolve:
// -1 - não está logado
// >=0 - está logado devolve o index
int is_user_registed(int pid){
    int i;
    for(i=0;i<servidor.online->numusers;i++){
        if (pid==servidor.online->users[i].pid) {
          return i;
        }
    }
    return -1;
}

//verifcar se o utilizador tem o login efectuado
//devolve:
// -1 - não está logado
// >=0 - está logado devolve o index
int is_user_loggedin(char *username){
    int i;
    for(i=0;i<servidor.online->numusers;i++){
        if (strcmp(username,servidor.online->users[i].username)==0) {
        return i;
        }
    }
    return -1;
}

// adiciona utilizadores ao array server.user_online
// devolve:
//   0-adicionado com sucesso
//   7-numero máximo de utilizadores atingidos
int user_loggedin(int pid, char *username) {
  int idx;
  idx= is_user_registed(pid);
  if (idx==-1) {
    if (servidor.online->numusers+1==MAXUSERS) return 7;
    strcpy(servidor.online->users[servidor.online->numusers].username,username);
    servidor.online->users[servidor.online->numusers].pid=pid;
    (servidor.online->numusers)++;
  } else {
    strcpy(servidor.online->users[idx].username,username);
  }
  return 0;
}

//faz logoff do user
// devolve
//  0- operação com sucesso
//  9- Utilizador não está autenticado
int user_loggedoff(int pid){
   int idx;
   if (servidor.online->numusers==0) return 9;

   idx=is_user_registed(pid);
   if (idx==-1) return 9;
   strcpy(servidor.online->users[idx].username,"");
   return 0;
}

//Remove o utilizador da lista de logados
// devolve
//  0- operação com sucesso
//  9- Utilizador não está autenticado
int user_exit(int pid){
   int idx;
   if (servidor.online->numusers==0) return 9;

   idx=is_user_registed(pid);
   if (idx==-1) return 9;

   //remove utilizador do array
   if (servidor.online->numusers-1>0) {
     servidor.online->numusers--;
     servidor.online->users[idx]=servidor.online->users[servidor.online->numusers];
   }

   return 0;
}


//Abre o pipe do servidor
//devolve
// 0 - operação com sucesso
// 10 - já existe o pipe criado do servidor
// 11 - Outro erro do SO
// 12 - Erro a abrir o fifo para leitura
int OpenFIFO(){
  int f;
  //printf("Cria e abre fifo...\n");

  if (access(FIFO_SERVIDOR, F_OK)){
    if (mkfifo(FIFO_SERVIDOR, 0777)!= 0) return 11;
  }

  f = open(FIFO_SERVIDOR, O_RDWR);
  if (f == -1){
    return 12;
  }
  servidor.fifo = f;
  //printf("Fifo aberto...\n");
  return 0;
}

//Fecha o FIFO
// devolve 0
int CloseFIFO(){
   close(servidor.fifo);
   return 0;
}


//envia resposa para o cliente
// entrada:
//   clienteid: pid do cliente para gerar nome do fifo
//   comando : comando a enviar
//   str: string a enviar
// saida:
//   0-operação com sucesso
//   22.
//   18.
int enviar_resposta(int clienteId, enum commands comando, char *str1, char *str2){
   	int ret;
    char fifo_name[FIFO_LEN];
    int fifo;
   	pedido ped;
   	ped.pid=clienteId;
   	ped.comando=comando;
    strcpy(ped.str1,str1);
    strcpy(ped.str2,str2);
    //printf("enviar resposta: %d, %s:%s\n",clienteId,str1,str2);
    //envia para o cliente

    sprintf(fifo_name,FIFO_CLIENTE,clienteId);

    fifo=open(fifo_name, O_RDWR);
    if (fifo==-1) {
        checkError(22,1);
    }
    ret=write(fifo, &ped, sizeof(pedido));
    close(fifo);
    kill(clienteId, SIGUSR1);
    //printf("enviado\n");

    return 0;
}

//envia mensagem para todos os clientes
int enviar_resposta_todos(int senderId, enum commands comando, char *str1, char *str2){
    int i;
    for (i=0;i<servidor.online->numusers; i++) {
        if (servidor.online->users[i].pid!=senderId) {
            enviar_resposta(servidor.online->users[i].pid,comando,str1,str2);
        }
    }
    return 0;
}

int enviar_status(int clienteId, int error){
   char erro[ERROR_LEN];
   getError(error,erro);
   return enviar_resposta(clienteId,c_status,"erro",erro);
}

//termina o servidor
void terminar(){
  int ret;

  enviar_resposta_todos(getpid(),c_server_exit,"servidor","terminado");

  CloseFIFO();
  ret=grava_utilizadores(servidor.registed->users,servidor.registed->numusers);
  checkError(ret,0);

  printf("\nEspera que os filhos terminem.\n");
  wait(NULL);

  //liberta memoria partilhada
  shmdt(servidor.online);
  shmdt(servidor.registed);

  //remove semaforo partilhado
  semctl(servidor.sem_msg,0,IPC_RMID);
  semctl(servidor.sem_online,0,IPC_RMID);
  semctl(servidor.sem_users,0,IPC_RMID);


//apaga os ficheiro os pipes
  system("rm pcli*");
  system("rm pserver*");
  printf("Servidor terminado\n");
  exit(0);
}

//Trata sinal
void procSinal(int signal) {
  if ((signal==SIGUSR1) || (signal==SIGINT)){
     terminar();
  }
}

void cli_register(pedido ped) {
  int idx;
  //printf("Regista cliente pid %d\n",ped.pid);
  semaforo(servidor.sem_online,-1);
  idx= is_user_registed(ped.pid);
  if (idx!=-1) {
     user_loggedin(ped.pid,"");
  }
  semaforo(servidor.sem_online,+1);
  enviar_resposta(ped.pid,c_status,"Registado","Faça login");
}

void cli_login(pedido ped) {
  int ret;
  semaforo(servidor.sem_users,-1);
  ret=login(ped.str1,ped.str2);
  semaforo(servidor.sem_users,+1);
  if (ret==0) {
      semaforo(servidor.sem_online,-1);
      user_loggedin(ped.pid,ped.str1);
      enviar_resposta_todos(ped.pid,c_status,ped.str1,"está online");
      semaforo(servidor.sem_online,+1);
      enviar_resposta(ped.pid,c_login,ped.str1,"Login com sucesso");
  } else if(ret==6){
		//caso o cliente nao exista regista
		semaforo(servidor.sem_users,-1);
		ret=add_user(ped.str1,ped.str2);
		semaforo(servidor.sem_users,+1);
		if( ret ==0 ){
		  enviar_resposta(ped.pid,c_login,ped.str1,"Registo e Login com sucesso");
		  semaforo(servidor.sem_online,-1);
		  user_loggedin(ped.pid,ped.str1);
		  enviar_resposta_todos(ped.pid,c_status,ped.str1,"está online");
		  semaforo(servidor.sem_online,+1);
		}
  }

  else {
      enviar_status(ped.pid,ret);
  }
}

void cli_logoff(pedido ped) {
  int idx;
  semaforo(servidor.sem_online,-1);
  idx=is_user_registed(ped.pid);
  if (idx!=-1)
    enviar_resposta_todos(ped.pid,c_status,servidor.online->users[idx].username,"está off-line");
  user_loggedoff(ped.pid);
  semaforo(servidor.sem_online,+1);
}

void cli_write(pedido ped) {
    int idx;
    int dest;
    int count=0;
    semaforo(servidor.sem_online,-1);
    idx=is_user_registed(ped.pid);
    if (idx>=0) {

        if (strcmp("all",ped.str1)==0) {
           count ++;
           enviar_resposta_todos(ped.pid,c_status,servidor.online->users[idx].username,ped.str2);
        } else
        {
            for(dest=0;dest<servidor.online->numusers;dest++){
                if (strcmp(ped.str1,servidor.online->users[dest].username)==0) {
                   count ++;
                   if (ped.pid!=servidor.online->users[dest].pid)
                     enviar_resposta(servidor.online->users[dest].pid,c_status,servidor.online->users[idx].username,ped.str2);
                }
            }
        }
    }

    semaforo(servidor.sem_online,1);
    if (count==0)  enviar_status(ped.pid,25);
}

void cli_remove(pedido ped) {
    int idx;
    semaforo(servidor.sem_online,-1);
    semaforo(servidor.sem_users,-1);

    idx=is_user_registed(ped.pid);
    remove_user(servidor.online->users[idx].username);
    user_loggedoff(ped.pid);
    enviar_resposta_todos(getpid(),c_status,ped.str1,"está off-line");

    semaforo(servidor.sem_users,+1);
    semaforo(servidor.sem_online,+1);

}

void cli_exit(pedido ped) {
    int idx;
    semaforo(servidor.sem_online,-1);
    idx=is_user_registed(ped.pid);
    if (idx!=-1)
      enviar_resposta_todos(ped.pid,c_status,servidor.online->users[idx].username,"está off-line");
    user_exit(ped.pid);
    semaforo(servidor.sem_online,+1);
}

//processa comandos do filho
void process_child(pedido ped){
   switch (ped.comando) {
      case c_register:
           cli_register(ped);
           break;
      case c_login:
           cli_login(ped);
           break;
      case c_logoff:
           cli_logoff(ped);
           break;
      case c_remove:
           cli_remove(ped);
           break;
      case c_write:
           cli_write(ped);
           break;
      case c_exit:
           cli_exit(ped);
           break;
      default:
        enviar_status(ped.pid,21);
   }
}

//motor do servidor
int processa_pedidos(){
  int res;
  pedido ped;
  do {
    //printf("Espera pedidos...\n");
    semaforo(servidor.sem_msg,-1);
    //printf("processa pedido...\n");
    if((res=read(servidor.fifo, &ped, sizeof(pedido))) == -1 ){
      checkError(13,1);
    }

    if (res>0){
        if (fork()==0) {
          //inicia processo filho para processamento e termina
          process_child(ped);
          exit(0);
        }
    }
  } while (1);
  return 0;
}

int inicia_servidor(){
	int ret;
    //criar memoria partilhada users online
    servidor.online_shmid = shmget(SHM_ONLINE,sizeof(USERONLINECTRL), IPC_CREAT  | 0777);
    if(servidor.online_shmid == -1)
        checkError(23,1);
   //mapear memoria partilhada
    if ( ( (servidor.online) =(PUSERONLINECTRL) shmat(servidor.online_shmid,(void *) 0, 0)) == (void *) -1) {
        checkError(24,1);
    }

    //criar memoria partilhada users
    servidor.users_shmid = shmget(IPC_PRIVATE,sizeof(USERCTRL),IPC_CREAT  | 0777);
    if(servidor.users_shmid == -1)
        checkError(23,1);

    if ( ( (servidor.registed) =(PUSERCTRL) shmat(servidor.users_shmid,(void *) 0, 0)) == (void *) -1) {
        checkError(24,1);
    }


    // inicia variavel servidor
    servidor.registed->numusers=0;
    servidor.online->numusers=0;
    servidor.fifo=0;
    servidor.sem_msg = semaforoInit(0,SEM_MSG,IPC_CREAT|0777);
    servidor.sem_users = semaforoInit(1,IPC_PRIVATE,IPC_CREAT|0777);
    servidor.sem_online = semaforoInit(1,SEM_ONLINE,IPC_CREAT|0777);

    //abrir ficheiro de utilizadores
    ret=abre_utilizadores(servidor.registed->users,&servidor.registed->numusers);
    if (ret!=0 && ret!=3) {
      checkError(ret,1);
    }

    if (signal(SIGUSR1,procSinal)==SIG_ERR) {
      checkError(14,1);
    }
    if (signal(SIGINT,procSinal)==SIG_ERR) {
      checkError(14,1);
    }

    ret=OpenFIFO();
    checkError(ret,1);

    //teste();

    ret=processa_pedidos();
    checkError(ret,1);
    terminar();
    return 0;
}

int main(int argc,char **argv) {
    int pid;
    //verifica se servidor está aberto
    if (access(FIFO_SERVIDOR, F_OK)!=-1){
        printf("\nServidor já está em execução\n");
        exit(1);
    }

	//verifica_servidor_aberto();
    pid=fork();
    if (pid==0) {
       return inicia_servidor();
       exit(0);
    } else
    if (pid==-1)  {
        checkError(26,1);
    }
    printf("\nServidor iniciado... para terminar: kill -10 %d\n",pid);
    return 0;
}

