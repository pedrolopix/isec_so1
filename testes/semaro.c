#include <stdio.h>


int main()
{
   union semum valor;
   struct sembuf op;
   
   

   //criar semaforo
   //IPC_PRIVATE só dá entre parentes
   //ou usar >0 (exemplo o meu bi) para usar entre processos.
   //IPC_EXCL -> se ja existe devolve -1; optional
   int id_sem=semget(IPC_PRIVATE),1,IPC_CREAT|0777|IPC_EXCL);

   //iniciar semaforo
   valor.val=1;
   semctrl(id_sem,0,setval,valor);

   //Usar semaforo;	
   op.sem_op = -1; // Acrescenta/decrementa
   op.sem_flg=SEM_UNDO;
   op.sem_num=0;
   semop(id_sem,%op,1); //1-numero de operações

}
