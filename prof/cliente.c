#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

struct pedido{
	char str[256];
	int pid;
};


int main(){
	int fifo1,fifo2;
	struct pedido ped;
	char nome_fifo[20];

	//Criar fifo de resposta
	ped.pid=getpid();
	sprintf(nome_fifo,"fifo_%d",getpid());
	if(access(nome_fifo,F_OK) == -1)
		mkfifo(nome_fifo,0777);

	//Abrir para escrita 
	if((fifo1=open("fifo_serv",O_WRONLY)) == -1){
		perror("Na abertura de fifo_serv");
		exit(-1);
	}

	//Ciclo de pedidos
	do{
		printf("Introduza uma frase:");
		fgets(ped.str,256,stdin);
		
		write(fifo1, &ped, sizeof(ped));

		//Abrir fifo de resposta
		fifo2=open(nome_fifo, O_RDONLY);
		read(fifo2, &ped, sizeof(ped));
		
		printf("Servidor respondeu:%s",ped.str);
	}while(strncmp(ped.str, "FIM",3));


	exit(0);
}
