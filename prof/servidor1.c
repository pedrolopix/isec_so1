#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
struct pedido{
	char str[256];
	int pid;
};


int main(){
	int fifo1,fifo2,i;
	struct pedido ped;
	char nome_fifo[20];

	//Se nao existir cria
	if(access("fifo_serv",F_OK) == -1)
		mkfifo("fifo_serv", 0777);
	
	//Abrir para leitura 
	if((fifo1=open("fifo_serv",O_RDONLY)) == -1){
		perror("Na abertura de fifo_serv");
		exit(-1);
	}

	//Ciclo de leitura
	do{
		printf("A espera de novo pedido...\n");		
		read(fifo1, &ped, sizeof(ped));
                if (fork()==0) {
			//if(strcmp(ped.str, "fim") == 0)
			//	break;
			//else{
				for(i=0;i<strlen(ped.str);i++)
				{
				   sleep(1);
				   ped.str[i]=toupper(ped.str[i]);	
				}					
				sprintf(nome_fifo, "fifo_%d", ped.pid);
				fifo2=open(nome_fifo, O_WRONLY);
				write(fifo2, &ped, sizeof(ped));
		                close(fifo2);
                                exit(0); //termina filho;
			//}
		}
	}while(1);


	exit(0);
}
